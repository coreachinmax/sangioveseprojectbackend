package com.example.sangioveseproject.controller;

import com.example.sangioveseproject.entity.Region;
import com.example.sangioveseproject.entity.Wine;
import com.example.sangioveseproject.request.RegionRequest;
import com.example.sangioveseproject.response.RegionResponse;
import com.example.sangioveseproject.response.WineResponse;
import com.example.sangioveseproject.service.RegionService;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/api/regions/")
public class RegionController {
    @Autowired
    RegionService regionService;

    @GetMapping("/{id}")
    public RegionResponse getRegion(@PathVariable long id) {
        Region region = regionService.getRegion(id);

        return new RegionResponse(region);
    }

    @GetMapping()
    public List<RegionResponse> getAllRegions() {
        List<Region> regions = regionService.getAllRegions();

        List<RegionResponse> regionsResponse = new ArrayList<>();
        regions.forEach(region -> {
            regionsResponse.add(new RegionResponse(region));
        });

        return regionsResponse;
    }

    @PostMapping()
    @ResponseStatus(HttpStatus.CREATED)
    public RegionResponse addRegion(@Valid @RequestBody RegionRequest regionRequest) {
        Region region = regionService.insertRegion(regionRequest);
        return new RegionResponse(region);
    }

    @PutMapping("/{id}")
    public Region updateRegion(@PathVariable long id, @Valid @RequestBody RegionRequest regionRequest) {
        return regionService.updateRegion(id, regionRequest);
    }

    @DeleteMapping("/{id}")
    public void deleteRegion(@PathVariable long id) {
        regionService.deleteRegion(id);
    }
}
