package com.example.sangioveseproject.controller;

import com.example.sangioveseproject.entity.Wine;
import com.example.sangioveseproject.request.WineRequest;
import com.example.sangioveseproject.response.WineResponse;
import com.example.sangioveseproject.service.WineService;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/api/wines/")
public class WineController {
    @Autowired
    WineService wineService;

    @GetMapping()
    public List<WineResponse> getAllWines(@RequestParam(required = false) String regionId, @RequestParam(required = false) String wineTypeId) {
        List<Wine> wines = wineService.getAllWines(regionId, wineTypeId);

        List<WineResponse> winesResponse = new ArrayList<>();
        wines.forEach(wine -> {
            winesResponse.add(new WineResponse(wine));
        });

        return winesResponse;
    }

    @GetMapping("/{id}")
    public WineResponse getWine(@PathVariable long id) {
        Wine wine = wineService.getWine(id);

        return new WineResponse(wine);
    }

    @PostMapping()
    @ResponseStatus(HttpStatus.CREATED)
    public WineResponse addWine(@Valid @RequestBody WineRequest wineRequest) {
        Wine wine = wineService.insertWine(wineRequest);
        return new WineResponse(wine);
    }

    @PutMapping("/{id}")
    public WineResponse updateWine(@PathVariable long id, @Valid @RequestBody WineRequest wineRequest) {
        Wine wine = wineService.updateWine(id, wineRequest);
        return new WineResponse(wine);
    }

    @DeleteMapping("/{id}")
    public void deleteWine(@PathVariable long id) {
        wineService.deleteWine(id);
    }
}
