package com.example.sangioveseproject.controller;

import com.example.sangioveseproject.entity.Wine;
import com.example.sangioveseproject.entity.WineType;
import com.example.sangioveseproject.request.WineTypeRequest;
import com.example.sangioveseproject.response.WineResponse;
import com.example.sangioveseproject.response.WineTypeResponse;
import com.example.sangioveseproject.service.WineTypeService;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/api/winetypes/")
public class WineTypeController {
    @Autowired
    WineTypeService wineTypeService;

    @GetMapping("/{id}")
    public WineTypeResponse getWineType(@PathVariable long id) {
        WineType wineType = wineTypeService.getWineType(id);

        return new WineTypeResponse(wineType);
    }

    @GetMapping()
    public List<WineTypeResponse> getAllWineTypes() {
        List<WineType> wineTypes = wineTypeService.getAllWineTypes();

        List<WineTypeResponse> wineTypesResponse = new ArrayList<>();
        wineTypes.forEach(wineType -> {
            wineTypesResponse.add(new WineTypeResponse(wineType));
        });

        return wineTypesResponse;
    }

    @PostMapping()
    @ResponseStatus(HttpStatus.CREATED)
    public WineTypeResponse addWineType(@Valid @RequestBody WineTypeRequest wineTypeRequest) {
        WineType wineType = wineTypeService.insertWineType(wineTypeRequest);
        return new WineTypeResponse(wineType);
    }

    @PutMapping("/{id}")
    public WineType updateWineType(@PathVariable long id, @Valid @RequestBody WineTypeRequest wineTypeRequest) {
        return wineTypeService.updateWineType(id, wineTypeRequest);
    }

    @DeleteMapping("/{id}")
    public void deleteWineType(@PathVariable long id) {
        wineTypeService.deleteWineType(id);
    }
}
