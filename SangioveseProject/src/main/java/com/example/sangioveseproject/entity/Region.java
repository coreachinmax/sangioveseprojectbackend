package com.example.sangioveseproject.entity;

import com.example.sangioveseproject.request.RegionRequest;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "regions")
public class Region {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @Column(name = "region_name")
    private String regionName;
    @OneToMany(mappedBy = "region")
    private List<Wine> wines;

    public Region(RegionRequest regionRequest) {
        regionName = regionRequest.getRegionName();
    }
}
