package com.example.sangioveseproject.entity;

import com.example.sangioveseproject.request.WineRequest;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "wines")
public class Wine {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @Column(name = "item_name")
    private String itemName;
    @Column(name = "description", length = 1000)
    private String description;
    @Column(name = "image_link")
    private String imageLink;
    @Column(name = "price")
    private int price;
    @ManyToOne
    private Region region;
    @ManyToOne
    private WineType wineType;

    public Wine(WineRequest wineRequest) {
        itemName = wineRequest.getItemName();
        description = wineRequest.getDescription();
        imageLink = wineRequest.getImageLink();
        price = wineRequest.getPrice();
    }
}
