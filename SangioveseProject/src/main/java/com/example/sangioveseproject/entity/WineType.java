package com.example.sangioveseproject.entity;

import com.example.sangioveseproject.request.RegionRequest;
import com.example.sangioveseproject.request.WineTypeRequest;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "wine_types")
public class WineType {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @Column(name = "wine_type_name")
    private String wineTypeName;
    @OneToMany(mappedBy = "wineType")
    private List<Wine> wines;

    public WineType(WineTypeRequest wineTypeRequest) {
        wineTypeName = wineTypeRequest.getWineTypeName();
    }
}
