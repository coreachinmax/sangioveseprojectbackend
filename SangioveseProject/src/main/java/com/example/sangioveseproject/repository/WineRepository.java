package com.example.sangioveseproject.repository;

import com.example.sangioveseproject.entity.Region;
import com.example.sangioveseproject.entity.Wine;
import com.example.sangioveseproject.entity.WineType;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface WineRepository extends CrudRepository<Wine, Long> {
    public List<Wine> findAllByRegionAndWineType(Region $region, WineType $wineType);
}
