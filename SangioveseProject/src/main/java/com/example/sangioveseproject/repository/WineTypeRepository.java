package com.example.sangioveseproject.repository;

import com.example.sangioveseproject.entity.WineType;
import org.springframework.data.repository.CrudRepository;

public interface WineTypeRepository extends CrudRepository<WineType, Long> {
}
