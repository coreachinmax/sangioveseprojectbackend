package com.example.sangioveseproject.request;

import jakarta.validation.constraints.NotBlank;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RegionRequest {
    @NotBlank
    private String regionName;
}
