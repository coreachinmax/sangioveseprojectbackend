package com.example.sangioveseproject.request;

import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotBlank;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class WineRequest {
    @NotBlank
    private String itemName;
    @NotBlank
    private String description;
    @NotBlank
    private String imageLink;
    @Min(value = 0L, message = "The value must be set and must be positive")
    private int price;
    @Min(value = 0L, message = "The value must be set and must be positive")
    private long regionId;
    @Min(value = 0L, message = "The value must be set and must be positive")
    private long typeId;
}
