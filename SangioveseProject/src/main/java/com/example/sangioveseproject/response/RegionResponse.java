package com.example.sangioveseproject.response;

import com.example.sangioveseproject.entity.Region;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RegionResponse {
    private long id;
    private String regionName;

    public RegionResponse(Region region) {
        id = region.getId();
        regionName = region.getRegionName();
    }
}
