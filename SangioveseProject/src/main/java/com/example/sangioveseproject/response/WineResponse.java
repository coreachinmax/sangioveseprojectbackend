package com.example.sangioveseproject.response;

import com.example.sangioveseproject.entity.Wine;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class WineResponse {
    private long id;
    private String itemName;
    private String description;
    private String imageLink;
    private int price;
    private WineTypeResponse wineType;
    private RegionResponse region;

    public WineResponse(Wine wine) {
        id = wine.getId();
        itemName = wine.getItemName();
        description = wine.getDescription();
        imageLink = wine.getImageLink();
        price = wine.getPrice();
        wineType = new WineTypeResponse(wine.getWineType());
        region = new RegionResponse(wine.getRegion());
    }
}
