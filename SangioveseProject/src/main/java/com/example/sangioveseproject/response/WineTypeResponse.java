package com.example.sangioveseproject.response;

import com.example.sangioveseproject.entity.WineType;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class WineTypeResponse {
    private long id;
    private String wineTypeName;

    public WineTypeResponse(WineType wineType) {
        id = wineType.getId();
        wineTypeName = wineType.getWineTypeName();
    }
}
