package com.example.sangioveseproject.service;

import com.example.sangioveseproject.entity.Region;
import com.example.sangioveseproject.entity.Wine;
import com.example.sangioveseproject.exception.ResourceNotFound;
import com.example.sangioveseproject.repository.RegionRepository;
import com.example.sangioveseproject.request.RegionRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RegionService {
    @Autowired
    RegionRepository regionRepository;

    public Region getRegion(long regionId) {
        return regionRepository.findById(regionId).orElseThrow(
                () -> new ResourceNotFound("provided id not found"));
    }

    public List<Region> getAllRegions() {
        return (List<Region>) regionRepository.findAll();
    }

    public Region insertRegion(RegionRequest regionRequest) {
        return regionRepository.save(new Region(regionRequest));
    }

    public Region updateRegion(long regionId, RegionRequest regionRequest) {
        return regionRepository.findById(regionId).map(region -> {
            Region regionToBeUpdated = new Region(regionRequest);
            regionToBeUpdated.setId(region.getId());
            return regionRepository.save(regionToBeUpdated);
        }).orElseThrow(()->new ResourceNotFound("Region ID not found"));
    }

    public void deleteRegion(long regionId) {
        regionRepository.findById(regionId).orElseThrow(()-> new ResourceNotFound("Region ID not found"));
        regionRepository.deleteById(regionId);
    }
}
