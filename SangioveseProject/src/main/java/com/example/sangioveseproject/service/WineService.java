package com.example.sangioveseproject.service;

import com.example.sangioveseproject.entity.Region;
import com.example.sangioveseproject.entity.Wine;
import com.example.sangioveseproject.entity.WineType;
import com.example.sangioveseproject.exception.ResourceNotFound;
import com.example.sangioveseproject.repository.RegionRepository;
import com.example.sangioveseproject.repository.WineRepository;
import com.example.sangioveseproject.repository.WineTypeRepository;
import com.example.sangioveseproject.request.WineRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class WineService {
    @Autowired
    WineRepository wineRepository;
    @Autowired
    RegionRepository regionRepository;
    @Autowired
    WineTypeRepository wineTypeRepository;

    public List<Wine> getAllWines(String regionId, String wineTypeId) {
        if ((regionId == null || regionId.isEmpty()) || (wineTypeId == null || wineTypeId.isEmpty())) {
            return (List<Wine>) wineRepository.findAll();
        } else {
            Region region = regionRepository.findById(Long.parseLong(regionId)).orElseThrow(
                    () -> new ResourceNotFound("provided region id not found"));
            WineType wineType = wineTypeRepository.findById(Long.parseLong(wineTypeId)).orElseThrow(
                    () -> new ResourceNotFound("provided wine type id not found"));
            return wineRepository.findAllByRegionAndWineType(region, wineType);
        }
    }

    public Wine getWine(long wineId) {
        return wineRepository.findById(wineId).orElseThrow(
                () -> new ResourceNotFound("provided id not found"));
    }

    public Wine insertWine(WineRequest wineRequest) {
        Region region = regionRepository.findById(wineRequest.getRegionId()).orElseThrow(
                () -> new ResourceNotFound("provided region id not found"));
        WineType wineType = wineTypeRepository.findById(wineRequest.getTypeId()).orElseThrow(
                () -> new ResourceNotFound("provided wine type id not found"));
        Wine wine = new Wine(wineRequest);
        wine.setRegion(region);
        wine.setWineType(wineType);
        return wineRepository.save(wine);
    }

    public Wine updateWine(long wineId, WineRequest wineRequest) {
        return wineRepository.findById(wineId).map(wine -> {
            Region region = regionRepository.findById(wineRequest.getRegionId()).orElseThrow(
                    () -> new ResourceNotFound("provided region id not found"));
            WineType wineType = wineTypeRepository.findById(wineRequest.getTypeId()).orElseThrow(
                    () -> new ResourceNotFound("provided wine type id not found"));
            Wine wineToBeUpdated = new Wine(wineRequest);
            wineToBeUpdated.setRegion(region);
            wineToBeUpdated.setWineType(wineType);
            wineToBeUpdated.setId(wine.getId());
            return wineRepository.save(wineToBeUpdated);
        }).orElseThrow(()->new ResourceNotFound("Wine ID not found"));
    }

    public void deleteWine(long wineId) {
        wineRepository.findById(wineId).orElseThrow(()-> new ResourceNotFound("Wine ID not found"));
        wineRepository.deleteById(wineId);
    }
}
