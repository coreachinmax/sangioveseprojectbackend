package com.example.sangioveseproject.service;

import com.example.sangioveseproject.entity.Wine;
import com.example.sangioveseproject.entity.WineType;
import com.example.sangioveseproject.exception.ResourceNotFound;
import com.example.sangioveseproject.repository.WineTypeRepository;
import com.example.sangioveseproject.request.WineTypeRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class WineTypeService {
    @Autowired
    WineTypeRepository wineTypeRepository;

    public WineType getWineType(long wineTypeId) {
        return wineTypeRepository.findById(wineTypeId).orElseThrow(
                () -> new ResourceNotFound("provided id not found"));
    }

    public List<WineType> getAllWineTypes() {
        return (List<WineType>) wineTypeRepository.findAll();
    }
    
    public WineType insertWineType(WineTypeRequest wineTypeRequest) {
        return wineTypeRepository.save(new WineType(wineTypeRequest));
    }

    public WineType updateWineType(long wineTypeId, WineTypeRequest wineTypeRequest) {
        return wineTypeRepository.findById(wineTypeId).map(wineType -> {
            WineType wineTypeToBeUpdated = new WineType(wineTypeRequest);
            wineTypeToBeUpdated.setId(wineType.getId());
            return wineTypeRepository.save(wineTypeToBeUpdated);
        }).orElseThrow(()->new ResourceNotFound("WineType ID not found"));
    }

    public void deleteWineType(long wineTypeId) {
        wineTypeRepository.findById(wineTypeId).orElseThrow(()-> new ResourceNotFound("WineType ID not found"));
        wineTypeRepository.deleteById(wineTypeId);
    }
    
}
