INSERT INTO regions (region_name)
VALUES ('France');
INSERT INTO regions (region_name)
VALUES ('Italy');
INSERT INTO regions (region_name)
VALUES ('Portugal');

INSERT INTO wine_types (wine_type_name)
VALUES ('Red');
INSERT INTO wine_types (wine_type_name)
VALUES ('White');
INSERT INTO wine_types (wine_type_name)
VALUES ('Green');

INSERT INTO wines (item_name, description, image_link, price, region_id, wine_type_id)
VALUES ('Chateau de Beaucastel Chateauneuf-du-Pape 2020',
        'Magnificent deep-color of an intense ruby-red. Typical of Beaucastel, the nose is rich and complex but very fresh, with an opulent aromatic bouquet dominated by notes of fresh fruit and spices. Full,-bodied, soft, eminently silky, this very elegant wine reveals a superb, dense structure on the palate, very fine tannins and gourmet aromas of fresh red fruit, currants and noble spices. The finish has great length, ample and harmonious. A fruity vintage with great aging potential.',
        'https://www.wine.com/product/images/w_1024,h_3325,c_fit,q_auto:good,fl_progressive/qwuufhgkt3k44ttwu5pe.jpg',
        12000, 1, 1);
INSERT INTO wines (item_name, description, image_link, price, region_id, wine_type_id)
VALUES ('Chateau Mouton Rothschild 2019',
        'The wine is an intense garnet red with purplish hue. Fresh, highly expressive and precise on the nose, it reveals blackberry, black cherry and licorice aromas with a slightly mineral cast. It is smooth and opulent on the palate, with an attractive sweetness, enfolding superbly patrician, rounded and powerful tannins. Beautifully rich overall, it culminates in a stylish, succulent and very harmonious finish.',
        'https://www.wine.com/product/images/w_480,h_600,c_fit,q_auto:good,fl_progressive/okdxfor3h3xyeailgrnk.jpg',
        76997, 1, 1);
INSERT INTO wines (item_name, description, image_link, price, region_id, wine_type_id)
VALUES ('Gaja Rossj-Bass Chardonnay 2020',
        'Golden color with greenish hues. This wine opens with an intense and intriguing nose of floral notes, wisteria and may flowers, plus hints of summer fruits as peach and melon. The aroma''s swiftly lead the way to honey and mineral notes. Rich, voluptuous and creamy, this Rossj-Bass also boasts a bright acidity that invites you back for sip after sip.',
        'https://www.wine.com/product/images/w_480,h_600,c_fit,q_auto:good,fl_progressive/wsvjto1dxtw6zhkly3ra.jpg',
        9699, 2, 2);
INSERT INTO wines (item_name, description, image_link, price, region_id, wine_type_id)
VALUES ('Antinori Marchese Chianti Classico Riserva 2019',
        'Marchese Antinori Chianti Classico Riserva is ruby red in color. On the nose, intense notes of ripe red fruit, cherries, currants, and raspberries lead over to delicate floral scents of dog rose, violets and lavender. Its bouquet is completed by typical sanguine sensations together with pleasant spicy hints of tobacco, cinnamon, cloves with a sweet note of milk chocolate. Its palate is well-balanced with lively, silky tannins that impart freshness and sustain a savory, persistent finish.',
        'https://www.wine.com/product/images/w_768,h_2194,c_fit,q_auto:good,fl_progressive/nfpgdt6qmqxpxu6kbb1f.jpg',
        4699, 2, 1);
INSERT INTO wines (item_name, description, image_link, price, region_id, wine_type_id)
VALUES ('Casa do Valle Vinho Verde Branco Grande Escolha 2020',
        'From the careful selection of the best grapes from their vineyards, the result is a straw colored wine, with an exuberant fruit driven aroma and minerality. On the palate the structure makes this wine complex, round, fresh and with a long and appealing finish.',
        'https://www.wine.com/product/images/w_480,h_600,c_fit,q_auto:good,fl_progressive/xgw1olkon6tbnspopska.jpg',
        1599, 3, 3);